package io.churina.exercise.service;

import io.churina.exercise.entity.Office;
import java.io.IOException;
import java.util.List;

public interface OfficeService {
   List<Office> getOffices() throws IOException;
}
