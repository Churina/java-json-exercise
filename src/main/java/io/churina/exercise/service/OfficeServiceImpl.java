package io.churina.exercise.service;

import io.churina.exercise.dao.OfficeData;
import io.churina.exercise.entity.Office;
import java.io.IOException;
import java.util.List;

public class OfficeServiceImpl implements OfficeService{

    private OfficeData officeData;

    public void setOfficeData(OfficeData officeData) {
        this.officeData = officeData;
    }

    @Override
    public List<Office> getOffices() throws IOException {
       return officeData.getOffices();
    }
}
