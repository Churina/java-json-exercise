package io.churina.exercise.service;

import io.churina.exercise.dao.EmployeeDao;

public class EmployeeServiceImpl implements EmployeeService{
    private EmployeeDao employeeDao;
    public void setEmployeeDao(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }
    @Override
    public void getEmployees() {
        employeeDao.getEmployees();
    }
}

