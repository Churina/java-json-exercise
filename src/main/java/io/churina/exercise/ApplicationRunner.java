package io.churina.exercise;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter;
import io.churina.exercise.controller.EmployeeController;
import io.churina.exercise.controller.OfficeController;
import io.churina.exercise.dao.EmployeeCsvData;
import io.churina.exercise.dao.EmployeeDao;
import io.churina.exercise.dao.EmployeeXmlData;
import io.churina.exercise.dao.OfficeDataImpl;
import io.churina.exercise.entity.Employee;
import io.churina.exercise.service.EmployeeService;
import io.churina.exercise.entity.Office;
import io.churina.exercise.service.EmployeeServiceImpl;
import io.churina.exercise.service.OfficeService;
import io.churina.exercise.service.OfficeServiceImpl;
import java.io.IOException;

import java.util.Date;
import java.util.List;

public class ApplicationRunner {
    public static void main(String[] args) throws IOException {
        EmployeeController employeeController = new EmployeeController();
        EmployeeServiceImpl employeeServiceImpl = new EmployeeServiceImpl();
        EmployeeCsvData employeeCsvData = new EmployeeCsvData();
        EmployeeXmlData employeeXmlData = new EmployeeXmlData();


        employeeController.setEmployeeService(employeeServiceImpl);
        employeeServiceImpl.setEmployeeDao(employeeCsvData);
        employeeController.start();
        employeeServiceImpl.setEmployeeDao(employeeXmlData);
        employeeController.start();



        // initialize json adapter
        Moshi moshi = new Moshi.Builder()
                .add(Date.class, new Rfc3339DateJsonAdapter())
                .build();

        OfficeController officeControllerImpl = new OfficeController();
        OfficeServiceImpl officeServiceImpl = new OfficeServiceImpl();
        OfficeDataImpl officeData = new OfficeDataImpl();

        officeControllerImpl.setOfficeService(officeServiceImpl);
        officeServiceImpl.setOfficeData(officeData);

        List<Office> officeList = officeControllerImpl.getOffices();

        JsonAdapter<Office> jsonOffice = moshi.adapter(Office.class);

        // loop through offices and write each out to the console
        for (Office office:officeList) {
            System.out.println(jsonOffice.toJson(office));
        }


    }
}
