package io.churina.exercise.entity;

public class Office {
    public String name;
    public String city;
    public String state;
    public String establishedDate;
}
