package io.churina.exercise.controller;

import io.churina.exercise.service.EmployeeService;
import io.churina.exercise.service.EmployeeServiceImpl;

public class EmployeeController {

    private EmployeeService employeeService;
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
    public void start(){
        employeeService.getEmployees();
    };
}
