package io.churina.exercise.controller;

import io.churina.exercise.service.OfficeService;
import io.churina.exercise.entity.Office;
import java.io.IOException;
import java.util.List;

public class OfficeController {
    private OfficeService officeService;

    public void setOfficeService(OfficeService officeService) {
        this.officeService = officeService;
    }

    public List<Office> getOffices() throws IOException {
      return officeService.getOffices();
    }
}
