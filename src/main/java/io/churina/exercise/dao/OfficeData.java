package io.churina.exercise.dao;

import io.churina.exercise.entity.Office;

import java.io.IOException;
import java.util.List;

public interface OfficeData {
    List<Office> getOffices() throws IOException;
}
