package io.churina.exercise.dao;

import io.churina.exercise.entity.Employee;

import java.util.List;

public interface EmployeeDao {
    List<Employee> getEmployees();
}
