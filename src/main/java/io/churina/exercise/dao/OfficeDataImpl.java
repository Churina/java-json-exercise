package io.churina.exercise.dao;

import io.churina.exercise.entity.Office;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OfficeDataImpl implements OfficeData{

    public List<Office> getOffices() throws IOException {
        String office1 = "{\"name\":\"ABC Inc.\",\"city\":\"Charlotte\",\"state\":\"NC\",\"establishedDate\":\"2000\"}";
        String office2 = "{\"name\":\"XYZ Inc.\",\"city\":\"Columbia\",\"state\":\"SC\",\"establishedDate\":\"1998\"}";


        Moshi moshi = new Moshi.Builder()
                .add(Date.class, new Rfc3339DateJsonAdapter())
                .build();
        JsonAdapter<Office> jsonAdapter = moshi.adapter(Office.class);

        List<Office> officeList = new ArrayList<>();

        officeList.add(jsonAdapter.fromJson(office1));
        officeList.add(jsonAdapter.fromJson(office2));

        return officeList;

    }
}
